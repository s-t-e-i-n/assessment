# Take Home Terraform Assessment

- [Take Home Terraform Assessment](#take-home-terraform-assessment)
  - [Problem Statement](#problem-statement)
  - [Provided Assumptions](#provided-assumptions)
  - [Additional Working Assumptions](#additional-working-assumptions)
  - [Prerequisites](#prerequisites)
  - [Directory Structure](#directory-structure)
  - [How To Use](#how-to-use)
  - [Choices and Trade-offs Made](#choices-and-trade-offs-made)
  - [How to Make This Better](#how-to-make-this-better)

## Problem Statement

Provide a set of Terraform files that allow the deployment of an application or
docker image to an AWS account.

Document steps necessary for:

1. User to authenticate and run the Terraform
2. Run the Terraform to create the resources
3. Validate the application/container has been deployed
4. Document choices and trade-offs made
5. Think about how you would improve this code

## Provided Assumptions

1. You have access to create required resources in AWS
2. Terraform state is stored in S3
3. Underlying network is not publicly available and is already created, so no
   resource creation is necessary for this piece, but you will need to make use
   of it in your code

## Additional Working Assumptions

1. The subnet mentioned above has a NAT gateway for outbound Internet access
2. You have the required VPN and access to reach the private network listed above
3. The application being deployed is a simple 'hello world' application
4. Your org uses AWS SSO for authentication. You have access to a development
   account but have not configured it locally
5. Your local dev environment runs MacOS

## Prerequisites

1. Run `./prerequisites.sh` to install applications in the Brewfile. These will
   be required to plan/apply/destroy the Terraform resources defined in this
   repo.

## Directory Structure

The `terraform/helloworld` directory contains the code used in this assessment.
See below for an overview of what each file is for:

```zsh
.
├── Brewfile
├── Brewfile.lock.json
├── README.md
├── prerequisites.sh
└── terraform
    └── helloworld
        ├── backend.tf                  # Terraform State configuration
        ├── container-linux-config.yaml # Base Container Linux Config (CLC)
        ├── data.tf                     # Terraform 'data' resources pulled from existing Terraform State
        ├── locals.tf                   # Terraform 'local' resources
        ├── main.tf                     # Terraform 'resource' resources to meet the problem statement
        ├── outputs.tf                  # Terraform 'output' resources     
        ├── provider.tf                 # Terraform 'provider' resources
        ├── snippets                    # Additional (CLC) files
        │   └── units.yaml              # Systemd unit files to install on the EC2 instance
        ├── variables.tf                # Terraform 'variable' resources
        └── version.tf                  # Terraform version definition
```

## How To Use

1. Complete the [prerequisite steps](#prerequisites)
2. Follow the steps below
```zsh
# Authenticate to AWS
aws sso login --profile helloworld-dev

# navigate to terraform directory
cd terraform/helloworld

# initialize terraform
tfenv install 1.2.5
tfenv use 1.2.5
terraform init

# review the terraform plan before applying
terraform plan

# create the resources
terraform apply
```
3. Using the `private_dns` output from `terraform apply`, verify the app is
   running. See below for an example command and output
```zsh
> curl <private_dns>:8080
No user header found
Server internal IP is: 172.17.0.2
```

## Choices and Trade-offs Made

This assessment could have been addressed in many ways. The approach taken here
was to create a single EC2 instance that runs a "Hello World" Docker image.

**Why?**

Simplicity was the main driver for the approach taken. Flatcar Container Linux
was chosen as the OS because it is a minimal image specifically designed to run
containers.

This approach does not take into account load balancing, auto-scaling, TLS
certificates, or custom Route53 entries because this was completed for a skills
assessment instead of a production application. That LOE requires additional
time commitment.

Also, most of this was tested on a personal AWS account, so using as cheap and
as minimal of resources as possible was a personal goal for this assessment.

**Trade-offs**

- Scalability - As mentioned earlier, this is not very scalable. If high volume
  is expected, this should be behind a load balancer of make use of an
  auto-scaling group.
- Monitoring - A monitoring agent(s) was not installed on this. One should be
  installed to verify the hello-world application is running
- EC2 instances - This requires management of an EC2 instance to run a "Hello
  World" app. Running this with Fargate might be less management (could be more
  expensive depending on use)
- Default resources - This assessment assumes the default VPC and subnets are
  used. It is not a good practice to use default VPC resources and instead
  create new resources with specified CIDRs.
- Flexibility - The Terraform in this assessment allows for some customization,
  but it also makes assumptions. It could be made more flexible by allowing
  other user inputs, however that requires additional time commitments to
  achieve.

## How to Make This Better

One way to make this better would be to have a system run all Terraform
commands. There are a number of ways to do this, for example
[Atlantis](https://www.runatlantis.io/), GitLab CI/CD, GitHub Actions, etc.
Using a system that automatically adds the plan output to a pull or merge
request is convenient for both the submitter and reviewers. This should also
lint the code for proper formatting.

If an organization already has modules for deploying an application, reusing
those modules would be beneficial and help keep infrastructure consistent.

If an organization requires tests, writing
[Terratest](https://terratest.gruntwork.io/) tests (or whatever the org uses)
should be done.

If an organization does not use AWS SSO, utilizing a utility like
[aws-vault](https://github.com/99designs/aws-vault) should be used to securely
handle AWS access keys and secret access keys.

Refactoring to address some of the trade-offs and limitations mentioned above
would also improve this.