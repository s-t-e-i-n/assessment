// Get the latest Flatcar Container Linux AMI
data "aws_ami" "flatcar" {
  most_recent = true
  owners      = ["075585003325"]

  filter {
    name   = "name"
    values = ["Flatcar-stable-*-hvm"]
  }
}

// Get the default VPC
data "aws_vpc" "default_vpc" {
  default = true
}

// Get a subnet based on the AZ and VPC
data "aws_subnets" "subnet" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default_vpc.id]
  }
  filter {
    name   = "availability-zone"
    values = [var.avability_zone]
  }
}