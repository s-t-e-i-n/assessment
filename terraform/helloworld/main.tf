// Define the security group for the 'helloworld' app
resource "aws_security_group" "helloworld" {
  name        = "helloworld"
  description = "Allowed inbound traffic to helloworld instance"
  vpc_id      = data.aws_vpc.default_vpc.id

  dynamic "ingress" {
    iterator = port
    for_each = var.allowed_tcp_ingress_ports
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_tcp_ingress_cidrs
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags, {})
}

// Validate container linux config and transpile to Ignition config
// for Flatcar
// https://registry.terraform.io/providers/poseidon/ct/latest/docs/data-sources/ct_config
data "ct_config" "helloworld" {
  content      = file("container-linux-config.yaml")
  strict       = true
  pretty_print = true

  snippets = [
    file("snippets/units.yaml"),
  ]
}

// Define the EC2 instance for the 'helloworld' app
resource "aws_instance" "helloworld" {
  ami           = data.aws_ami.flatcar.image_id
  instance_type = "t3.micro"

  root_block_device {
    volume_size = 10
  }

  key_name  = var.key_name
  user_data = data.ct_config.helloworld.rendered

  subnet_id              = data.aws_subnets.subnet.ids[0]
  vpc_security_group_ids = [aws_security_group.helloworld.id]

  tags = merge(local.common_tags, {})
}