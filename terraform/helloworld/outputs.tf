output "private_dns" {
  value = aws_instance.helloworld.private_dns
}

output "container_linux_config" {
  value = data.ct_config.helloworld.rendered
}