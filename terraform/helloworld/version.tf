terraform {
  required_version = "= 1.2.5"
  required_providers {
    aws = {
      version = ">= 4.22.0"
      source  = "hashicorp/aws"
    }
    ct = {
      source  = "poseidon/ct"
      version = "0.10.0"
    }
  }
}
