variable "avability_zone" {
  default     = "us-west-2a"
  description = "Avability Zone to place the EC2 instance"
}

variable "ami" {
  default     = "ami-03967149a3e8ae59d"
  description = "Flatcar Container Linux Stable AMI"
}

variable "key_name" {
  default     = null
  description = "The SSH Key pair name, if desired"
}

variable "allowed_tcp_ingress_ports" {
  default     = [8080]
  description = "List of TCP ingress ports to allow"
}

variable "allowed_tcp_ingress_cidrs" {
  default     = ["0.0.0.0/0"]
  description = "List of TCP ingress cidrs to allow"
}

variable "provider_profile" {
  default     = "helloworld-dev"
  description = "The AWS profile used by the AWS provider"
}