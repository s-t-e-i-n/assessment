#!/bin/zsh

set -e

# Install homebrew if it doesn't exist
if ! type brew > /dev/null; then
    echo "'brew' installation not found, installing Homebrew..."
    /bin/zsh -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "'brew' installation found, not installing Homebrew..."
fi

echo "Installing Brewfile..."
brew bundle

echo "Adding AWS SSO Auth config..."
mkdir -p ~/.aws/
cat << EOF >> ~/.aws/config
[profile helloworld-dev]
sso_start_url = https://helloworld.awsapps.com/start
sso_region = us-west-2
sso_role_name = AWSAdministratorAccess
sso_account_id = 123456789012
region = us-west-2
EOF
